/* define variables */
const pass = document.querySelector("#pwd");
const confPass = document.querySelector("#confpwd");
const passes = [pass, confPass];
const phoneNumber = document.querySelector("#phone");

/* do some password validation */
confPass.addEventListener('input', event => {
    if (pass.value === confPass.value) {
        passes.forEach(element => {
            element.style.borderColor = 'darkgray';
            element.onmousemove = function () {
                element.style.boxShadow = "0px 0px 1px blue";
                element.style.borderColor = 'blue';
            }
            element.onmouseout = function () {
                element.style.boxShadow = 'none';
                element.style.borderColor = 'darkgray';
            }
        });
    }
    else {
        passes.forEach(element => {
            element.style.borderColor = 'red';
            element.onmouseover = function () {
                element.style.boxShadow = "0px 0px 1px red";
            }
            element.onmouseout = function () {
                element.style.boxShadow = 'none';
            }
        });
    }
});

phoneNumber.addEventListener('input', event => {
    if (phoneNumber.value.length === 3) {
        phoneNumber.value = '(' + phoneNumber.value + ') ';
    }
    if (phoneNumber.value.length === 9) {
        phoneNumber.value = phoneNumber.value + '-';
    }
    if (isNaN(parseFloat(phoneNumber.value.slice(-1))) && 
        phoneNumber.value.slice(-1) != " " && 
        phoneNumber.value.slice(-1) != "(" && 
        phoneNumber.value.slice(-1) != ")" &&
        phoneNumber.value.slice(-1) != "-") {
        phoneNumber.value = phoneNumber.value.substring(0, phoneNumber.value.length -1);
    }
});